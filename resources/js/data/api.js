import React from "react";
import axios from "axios";

const addTodo = async ({ todoObject }) => {
    return await axios({
        method: "post",
        url: "/api/todos",
        data: {
            title: "Shooting with John Wick",
            description: "Shooting with John Wick"
        }
    });
};

const updateTodo = ({ todoObject, id }) => {};

const deleteTodo = ({ id }) => {};

const getTodos = async () => {
    return await axios.get("/api/todos");
};

const getTodoById = ({ id }) => {};

export { getTodoById, getTodos, deleteTodo, updateTodo, addTodo };
