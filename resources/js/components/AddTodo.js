import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import { addTodo } from "../data/api";
import { isNull } from "lodash";
import { isEmpty } from "../helper/inputHelper";

function AddTodo() {
    const [title, setTitle] = useEffect("");
    const [description, setDescription] = useEffect("");

    const handleFieldChange = ({ text, field }) => {
        if (!isEmpty(text) && field === "title") {
            setTitle(text);
        }

        if (!isEmpty(text) && field === "desc") {
            setTitle(text);
        }
    };

    const clearFields = () => {
        setTitle("");
        setDescription("");
    };

    return (
        <div className="main">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Add Todo</div>
                        <div className="input-group">
                            <input
                                type="text"
                                name="title"
                                placeholder="Title"
                                value={title}
                                onChange={text =>
                                    handleFieldChange(text, "title")
                                }
                            />
                            <input
                                type="text"
                                name="Description"
                                placeholder="Description"
                                value={description}
                                onChange={text =>
                                    handleFieldChange(text, "desc")
                                }
                            />
                            <div className="input-group-append">
                                <button
                                    className="btn btn-primary"
                                    onClick={() =>
                                        addTodo({
                                            title: title,
                                            description: description
                                        })
                                    }
                                >
                                    Add
                                </button>
                                <button
                                    onClick={clearFields}
                                    className="btn btn-primary"
                                >
                                    Back
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AddTodo;
