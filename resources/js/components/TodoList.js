import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import { getTodos } from "../data/api";
import Table from 'react-bootstrap/Table';
function TodoList() {
    const [todos, setTodos] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        todoList();
    }, []);

    const todoList = () => {
        setLoading(true);
        getTodos()
            .then(res => {
                console.log(res);
                setTodos(res.data);
                setLoading(false);
            })
            .catch(err => {
                setLoading(false);
            });
    };

    const renderBody = () => {
        return todos.map(todo => {
            return (
                <tr>
                    <td scope="row">{todo.id}</td>
                    <td>{todo.title}</td>
                    <td>{todo.description}</td>
                </tr>
            );
        });
    };

    return (
        <div className="main">
            <div className="row justify-content-center">
                <h1 className={`mb-5`}>Todo App</h1>
                {loading && <span className={`h4}`}>...loading</span>}
                <Table striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Description</th>
                            {/* <th scope="col">Handle</th> */}
                        </tr>
                    </thead>
                    <tbody>{renderBody()}</tbody>
                </Table>
            </div>
        </div>
    );
}

export default TodoList;
