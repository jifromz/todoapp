import React, { useEffect } from "react";
import ReactDOM from "react-dom";

function UpdateTodo(){

    const [title, setTitle] = useEffect("");
    const [description, setDescription] = useEffect("");

    const handleFieldChange = ({ text }) => {};

    const clearFields = () => {
        setTitle("");
        setDescription("");
    };

    

    return (
        <div className="main">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Add Todo</div>
                        <div className="input-group">
                            <input
                                type="text"
                                name="title"
                                placeholder="Title"
                                value={title}
                                onChange={text => handleFieldChange(text)}
                            />
                            <input
                                type="text"
                                name="Description"
                                placeholder="Description"
                                value={title}
                                onChange={text => handleFieldChange(text)}
                            />
                            <div className="input-group-append">
                                <button className="btn btn-primary">Add</button>
                                <button
                                    onClick={clearFields}
                                    className="btn btn-primary"
                                >
                                    Back
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default UpdateTodo;
