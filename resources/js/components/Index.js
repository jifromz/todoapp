import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import TodoList from './TodoList';
import AddTodo from './AddTodo';
import UpdateTodo from './UpdateTodo';

function App() {
    return (
        <BrowserRouter>
            <div>
                
                <Switch>
                    <Route exact path="/" component={TodoList} />
                    <Route path='/create' component={AddTodo} />
                    <Route path='/:id' component={UpdateTodo} />
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;

if (document.getElementById("todos")) {
    ReactDOM.render(<App />, document.getElementById("todos"));
}
