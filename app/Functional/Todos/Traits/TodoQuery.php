<?php

namespace App\Functional\Todos\Traits;
use Illuminate\Support\Facades\DB;

trait TodoQuery{
    public function GetTodoById(){
        return Todo::find($id)->first();
    }
}