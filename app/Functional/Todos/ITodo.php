<?php

namespace App\Functional\Todos;

interface ITodo{
    public function AddTodo(Request $request);
    public function GetTodos();
    public function UpdateTodo(Request $request, $id);
    public function DeleteTodo($id);
}