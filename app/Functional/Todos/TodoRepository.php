<?php

namespace App\Functional\Todos;
use App\Todo;
use Illuminate\Support\Facades\DB;
use App\Functional\Todos\ITodo;
use App\Functional\Todos\Services;
use App\Functional\Todos\Traits\TodoQuery;

 class TodoRepository implements ITodo{

    use TodoQuery;
    protected $todo;

     public function __construct(Todo $todo)
     {
        $this->todo = $todo;
    }

    public function GetTodos()
    {
        return DB::table('todos')->get();
    }

    public function GetTodoById($id)
    {
        return Todo::find($id)->first();
    }

    public function UpdateTodo(Request $request, $id)
    {
        $todo = Todo::find($id);
        if($todo){
            $todo->title = $request->input('title');
            $todo->description = $request->input('description');
            $purchaser->save();
        }
        return $todo;
    }

    public function AddTodo(Request $request)
    {
        $todo = Todo::create([
            'title' => $request->title,
            'description' => $request->description
        ]);
        return $todo;
    }

    public function DeleteTodo($id)
    {
        $todo = Todo::find($id);
        if($todo){
            $destroy = Todo::destroy($id);
        }
        if ($destroy){

            $data=[
                'status'=>'1',
                'msg'=>'success'
            ];
        
        }else{
        
            $data=[
                'status'=>'0',
                'msg'=>'fail'
            ];
        }
        return $data;
    }
 }